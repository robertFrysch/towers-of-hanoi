#include <array>
#include <iostream>
#include <optional>
#include <thread>
#include <vector>

constexpr int mathModulo(int x, int m) noexcept { return (x % m + m) % m; }
constexpr bool flipBool(bool& b)       noexcept { b = !b; return b; }

class TowersOfHanoi
{
    struct Disk;
public:
    explicit TowersOfHanoi(unsigned long numDisks)
        : m_state{numDisks}
        , m_numIterationsLeft{(std::uint64_t{1u} << numDisks) - 1u}
    {
        if(numDisks > 63u)
            throw std::invalid_argument{"The maximum number of disks is 63."};
    }

    std::uint64_t numIterationsLeft() const noexcept { return m_numIterationsLeft; }
    const std::vector<Disk>& state()  const noexcept { return m_state; }

    void iterate() noexcept
    {
        for(auto isOddDisk = true; auto& disk : m_state)
        {
            if(disk.move(flipBool(isOddDisk))) break;
        }
        --m_numIterationsLeft;
    }

private:
    std::vector<Disk> m_state;
    std::uint64_t m_numIterationsLeft;

    struct Disk
    {
        int position = 0;
        bool wasMoved = false;

        bool move(bool oddSize) noexcept
        {
            if(flipBool(wasMoved))
                position = mathModulo(oddSize ? ++position : --position, 3);
            return wasMoved;
        }
    };
};

[[nodiscard]] std::string format(const TowersOfHanoi& towers)
{
    const auto numDisks = towers.state().size();
    auto stacks = std::array<std::vector<std::size_t>, 3u>{};
    for(auto& stack : stacks)
        stack.reserve(numDisks);
    for(auto d = 0u; d < numDisks; ++d)
        stacks.at(towers.state().at(numDisks - 1 - d).position).push_back(numDisks - d);
    for(auto& stack : stacks)
        stack.resize(numDisks, 0u);

    thread_local std::string ret;
    ret.clear();
    ret += "\n\n";

    std::string colLine;
    const auto columnSize = numDisks + 1u;
    colLine.reserve(columnSize);

    for(auto row = 0u; row < numDisks; ++row)
    {
        for(auto col = 0u; col < 3u; ++col)
        {
            colLine.clear();
            colLine.append(stacks[col].at(numDisks - 1u - row), '#');
            colLine.resize(columnSize, ' ');
            std::copy(colLine.rbegin(), colLine.rend(), std::back_inserter(ret));
            std::copy(colLine.begin(),  colLine.end(),  std::back_inserter(ret));
        }
        ret += '\n';
    }
    ret += '\n';
    return ret;
}

std::optional<unsigned long> parseNumDisks() noexcept
try
{
    std::string input;
    std::getline(std::cin, input);
    return std::stoul(input);
} catch (...) {
    return std::nullopt;
}

auto parseDelay(const char* input) noexcept
try
{
    return std::stoull(input);
} catch (...) {
    return 750ull;
}


int main(int argc, char* argv[])
{
    auto printUpdate = [delay_ms = parseDelay(argc > 1 ? argv[1] : "")](const TowersOfHanoi& towers) {
        std::cout << format(towers)
                  << towers.numIterationsLeft() << " moves left." << std::endl;
        std::this_thread::sleep_for(std::chrono::milliseconds(delay_ms));
    };

    while(true)
    {
        try
        {
            const auto numDisks = 3ul;
            auto towers = TowersOfHanoi{numDisks};

            std::cout << format(towers)
                      << "Type the number of disks for the Towers of Hanoi or press Enter for '3': ";
            if(const auto customNumDisks = parseNumDisks(); customNumDisks.has_value())
                towers = TowersOfHanoi{*customNumDisks};

            while(towers.numIterationsLeft() > 0u)
            {
                printUpdate(towers);
                towers.iterate();
            }
            std::cout << format(towers)
                      << "Towers of Hanoi accomplished!\n"
                      << "-----------------------------\n\n\n" << std::endl;
        } catch(const std::exception& e)
        {
            std::cerr << e.what() << '\n';
        }
    }
}
